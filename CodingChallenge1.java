package com.hcl.demo;

import java.util.Scanner;

public class CodingChallenge1 {

	public static void main(String[] args) {

		// As a user I should be able to print my name on the screen
		Scanner sc = new Scanner(System.in);
		String name = new String();
		System.out.println("please enter your name:");
		name = sc.nextLine();
		System.out.println("hello mr." + name);

		// As a user I should be able to list at least 5 task for the day.
		System.out.println("enter no of tasks:");
		int no_of_tasks = sc.nextInt();
		no_of_tasks += 1;
		String[] my_task = new String[no_of_tasks];
		System.out.println("ENTER TASKS:");
		for (int i = 0; i < my_task.length; i++) {
			my_task[i] = sc.nextLine();
		}
		System.out.println("--------the tasks are:---------");
		for (int i = 0; i < my_task.length; i++) {
			System.out.println(my_task[i]);
		}
		// As a user I should be able to see all the task in increasing and decreasing
		// order.
		String temp;
		for (int i = 0; i < my_task.length; i++) {
			for (int j = i + 1; j < my_task.length; j++) {
				if (my_task[i].compareTo(my_task[j]) > 0) {
					temp = my_task[i];
					my_task[i] = my_task[j];
					my_task[j] = temp;

				}
			}
		}
		// As a user I should be able to see all the task in increasing and decreasing
		// order.
		System.out.println();
		System.out.println("tasks sorted in ascending order: ");
		for (int i = 0; i < my_task.length; i++) {
			System.out.println(my_task[i]);
		}

		System.out.println("tasks sorted in decending order: ");
		for (int i = my_task.length - 1; i >= 0; i--) {
			System.out.println(my_task[i]);
		}
//As a user I should be able to see the repeated tasks.
		for(int i=0;i<my_task.length;i++)
		{
			int count=0;
			for(int j=i+1;j<my_task.length;j++)
			{
				if(my_task[i].compareTo(my_task[j])==0)
				{
					count+=1;
				}
			}
		if(count>0)
		{
			System.out.println("task "+my_task[i]+" is repeated "+count+" times");
		}
		}

	}
}
