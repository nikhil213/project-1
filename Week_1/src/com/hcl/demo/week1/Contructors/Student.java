package com.hcl.demo.week1.Contructors;

public class Student {

			private int sid;
			private String name;
			private double per;
			/**
			 * @param sid
			 * @param name
			 * @param per
			 */
			public Student(int sid, String name, double per) {
				super();
				this.sid = sid;
				this.name = name;
				this.per = per;
			}
			public int getSid() {
				return sid;
			}
			public void setSid(int sid) {
				this.sid = sid;
			}
			public String getName() {
				return name;
			}
			public void setName(String name) {
				this.name = name;
			}
			public double getPer() {
				return per;
			}
			public void setPer(double per) {
				this.per = per;
			}
		}
