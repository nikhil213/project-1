package com.hcl.demo.week1;

public class DataTypesDemo {

	public static void main(String[] args) {

		// there are 8 data types:These are primitive data types
		int integerNum = 63;
		float FloatNum = 9;
		boolean boolVal = false;
		char singleChar = 'n';
		short shortVal = 3;
		long longVal = 987654;
		double doubleVal = 87489.879;
		byte byteVal = 1;
		
		System.out.println(integerNum);
		System.out.println(FloatNum);
		System.out.println(boolVal);
		System.out.println(singleChar);
		System.out.println(shortVal);
		System.out.println(longVal);
		System.out.println(doubleVal);
		System.out.println(byteVal);
	}

}
