package com.hcl.demo.week1;
import java.util.Scanner;

public class sum_of_n_numbers {

	public static void main(String[] args) {
		int n,sum=0,i;
		Scanner s=new Scanner(System.in);
		System.out.println("\nenter a number:\n");
		n=s.nextInt();
		for(i=1;i<=n;i++)
		{
			sum+=i;
		}
		s.close();
		System.out.println("\nthe sum of "+n+" numbers is: "+sum);
	}
}
